import org.junit.Assert;
import org.junit.Test;
import org.openqa.selenium.Dimension;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

public class TesteGoogle {
	
	//String title = "Google";
	
	@Test
	public void teste() {
		WebDriver driver = new ChromeDriver();
		driver.manage().window().setSize(new Dimension(1200,765));
		driver.get("http://www.google.com");
		Assert.assertEquals("Google", driver.getTitle());
		//mantando a instancia do driver
		driver.quit();
	}
}
